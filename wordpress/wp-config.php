<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'databasewp');

/** MySQL database username */
define('DB_USER', 'wpuser');

/** MySQL database password */
define('DB_PASSWORD', 'wp123');

/** MySQL hostname */
define('DB_HOST', '10.0.0.222');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'GgUwDY1FU#jNlRs.#V@lc{Y9d$&U=Ab+fhjof/( iuW5Z<<&9)J|Q7V2viC25)}Z');
define('SECURE_AUTH_KEY',  'gdwi2wF=Klx.2:/uJ*U#/E8{|c6`J5s$tWd<#N#IU!~2V^u(aP4:>`6Abv}:YPCG');
define('LOGGED_IN_KEY',    '!vhe]tu+[@?m2g@s4o9DaVkbV^KRpKIip5mRJ1$vPyV*S]RvG$ x]%/-Af=|s0a#');
define('NONCE_KEY',        '@|dt{r9ewZ:alRLztP) o`kezeh$|vV`:l>5PR^<g$F4[.[? Y@pBiB>i~n]$??h');
define('AUTH_SALT',        '#(l$})~z?2GrHjg^h8Aukf19Ui,`W;QPn/+J@_S#z-q>5s%:f/m[,rL|8i)Sz-6p');
define('SECURE_AUTH_SALT', 'zI|unYoA?bzn[tN5)upvQ_&qLWU^LN,<eIl`7Xtkc|!ZW(;MVdiA;6<[-YJ7694)');
define('LOGGED_IN_SALT',   'gV-3)Q$P>]VNp1p;I%pd;ePs7D&c*JTR`#1(qCJl<)-pZI8&*ajh>Dwc7~bn* YN');
define('NONCE_SALT',       '~D{kTP_6VuZ:[PX7&i~pR<<yp&r1/ZaX|WcZw@E4rk|fQKmP=o_c)h:1[cH.Oqq+');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
