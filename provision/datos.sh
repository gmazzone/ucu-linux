export DEBIAN_FRONTEND=noninteractive

# Actualizacion e instalacion
apt-get update
apt-get -y install vim mysql-server dnsutils

# Configura mysql y DB
mysqladmin -u root password password
mysql -u root -ppassword -e "CREATE DATABASE IF NOT EXISTS databasewp;"
mysql -u root -ppassword -e "use databasewp;"
mysql -u root -ppassword -e "CREATE USER wpuser@localhost;"
mysql -u root -ppassword -e "SET PASSWORD FOR wpuser@localhost = PASSWORD('wp123');"

cp /vagrant/provision/my.cnf /etc/mysql/
chmod 644 /etc/mysql/my.cnf

mysql -u root -ppassword databasewp < /vagrant/provision/dump.sql
mysql -u root -ppassword -e "GRANT ALL PRIVILEGES ON databasewp.* TO 'wpuser'@'10.0.0.210' IDENTIFIED BY 'wp123';"
mysql -u root -ppassword -e "flush privileges;"

systemctl restart mysql

grep -F "10.0.0.210" /etc/hosts || echo "10.0.0.210 server" >> /etc/hosts
grep -F "10.0.0.222" /etc/hosts || echo "10.0.0.222 datos" >> /etc/hosts
grep -F "10.0.0.217" /etc/hosts || echo "10.0.0.217 control" >> /etc/hosts


cp /vagrant/provision/sshd_config  /etc/ssh/sshd_config

sudo useradd -m -ppassword -G sudo -s /bin/bash operador


 
