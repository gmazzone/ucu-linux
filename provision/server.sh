export DEBIAN_FRONTEND=noninteractive

apt-get update
apt-get -y install vim apache2 php5 php5-mysql php5-gd dnsutils mysql-client

rm -rf /var/www/html
ln -s /vagrant/wordpress /var/www/html

grep -F "10.0.0.210" /etc/hosts || echo "10.0.0.210 server" >> /etc/hosts
grep -F "10.0.0.222" /etc/hosts || echo "10.0.0.222 datos" >> /etc/hosts
grep -F "10.0.0.217" /etc/hosts || echo "10.0.0.217 control" >> /etc/hosts

cp /vagrant/provision/sshd_config  /etc/ssh/sshd_config

sudo useradd -m -ppassword -G sudo -s /bin/bash operador



